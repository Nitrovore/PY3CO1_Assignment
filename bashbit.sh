#!/bin/bash
echo Hello from the other side

TEMP=100 #Starting Temperature
TSTEP=100 #Percentage that the temperature is multiplied by at each step

ARRAYX=160 #System X dimension
ARRAYY=160  #System Y dimension

COUNTER=0
#while [  $TEMP -gt 0 ]; do
while [  $COUNTER -lt 60 ]; do
	python Test.py $TEMP $COUNTER $ARRAYX $ARRAYY
	#echo $(($RANDOM%2))
        let TEMP=$(($TSTEP*$TEMP/100))
	let COUNTER=$((COUNTER+1))
done






