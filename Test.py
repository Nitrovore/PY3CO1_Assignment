#!/usr/bin/python
import numpy as np
import scipy as sp
import matplotlib.pylab as plt
import math as mt
import random as rm
import sys

Kb = 8.61733E-5 #ev/K

j = 1 #ev
t = int(sys.argv[1]) #K
B = 1.0/(Kb*t)
Counter = int(sys.argv[2])
length = int(sys.argv[3]) #Loading data from the bash script with the sys module
width = int(sys.argv[4])

if Counter == 0:  #This sets magnetic field = 0.2 for first run
	h = 0.2
else:
	h = np.load('/tmp/12346.npy') #And whatever it was last time for the other runs



if Counter == 0:
	spinarray = np.zeros((length,width)) #Creating the array of spins on first run
	
	for i in range(0,(width*length)):
		a = (i//width)
		b = (i % width)
		spinarray[a,b] = rm.choice([-1,-1,1,1,1]) #A majority of spins are up giving
								#an initial magnetic field
		x = spinarray[a,b]
		



if Counter != 0:      #Subsequent runs load the array of the previous run
	spinarray = np.load('/tmp/12345.npy')
	




newspinarray = np.zeros((length,width))

def deltae(x,b,a,j): #The function used to find the energy change of a switch

	if a == length+1:
		above = 0 	#Compare to atom above, if on top edge set "above" = 0
	else:
		above = spinarray[a-1,b]

	if a == width+1:
		left = 0	#Likewise for the one on the left
	else:
		left = spinarray[a,b-1]
	if a == length-1:
		below = 0
	else:
		below = spinarray[a+1,b]	#Below

	if b == width-1:
		right = 0
	else:
		right = spinarray[a,b+1] 	#And to the right
	dE = ((-j*x)*(above+left+right+below))-(h*x) #And calculate change in energy from this
	return dE





f = open("TEST = {}.pbm".format((Counter+1)),"w") #Opening the .pbm file for creation
f.write("P1\n")				#Adding the first bit
f.write("{} {}\n".format(length,width))#Stating the dimension
	

m = 0.0
q = 1.0
for n in range(0,(width*length)):
	b = (n % width)
	a = (n//width) #a and b determine the position of the particular dot
	x = spinarray[a,b]
	m = m + x
	de = deltae(x,b,a,j)
	r = (rm.randint(0,1000)/1000.0) # A random number 0<r<1
	if  de <=0:
		p = 1.00   #Probabaility of switch P = 1.00 if energy change is negative
	else: p = np.exp(-B*de)  #Or this expression if it isn't
	if p >= r:		#Comparing P to random r decides if change is made
		spinarray[a,b] = -1*x
	else: 
		spinarray[a,b] = x

	
	z = str(int((spinarray[a,b]+1)/2.0)) #Converting -1,1 into 0,1
	if b == 0:
		f.write("\n")
		f.write(z)
	else:
		f.write(" ")
		f.write(z)



file.close

h = (m/(width*length))

np.save('/tmp/12345', spinarray)
np.save('/tmp/12346', h)

if abs(h) <0.02:
	print Counter

